import sys
import re
import urllib2
import urlparse

tocrawl = set(['http://docs.python.org/'])
#tocrawl = set([sys.argv[1]])
crawled = set([])
keywordregex = re.compile('<meta\sname=["\']keywords["\']\scontent=["\'](.*?)["\']\s/>')
linkregex = re.compile('<a\s*href=[\'|"](.*?)[\'"].*?>')

while 1:
	try:
		crawling = tocrawl.pop()
		print "crawling " + crawling
	except KeyError:
		raise StopIteration
	
	url = urlparse.urlparse(crawling)
	
	try:
		response = urllib2.urlopen(crawling)
	except:
		print crawling
	
	msg = response.read()
	startPos = msg.find('<title>')
	if startPos != -1:
		endPos = msg.find('</title>', startPos+7)
		if endPos != -1:
			title = msg[startPos+7:endPos]
			print "Title: " + title
	
	keywordlist = keywordregex.findall(msg)
	if len(keywordlist) > 0:
		keywordlist = keywordlist[0]
		keywordlist = keywordlist.split(", ")
		print "Keyword List: " + keywordlist
	
	links = linkregex.findall(msg)
	crawled.add(crawling)
	for link in (links.pop(0) for _ in xrange(len(links))):
		if link[0].startswith('/'):
			link = 'http://' + url[1] + link
		elif link[0].startswith('#'):
			link = 'http://' + url[1] + url[2] + link
		elif not link[0].startswith('http'):
			link = 'http://' + url[1] + '/' + link
		if link[0] not in crawled:
			tocrawl.add(link)
